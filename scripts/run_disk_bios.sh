#!/usr/bin/env bash

# Copyright Capes-OS Contributors:
#
# - Rémy Grünblatt <remy@grunblatt.org>
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

if [ "$#" -ne 1 ]; then
	    echo "Illegal number of parameters. Usage: ./run_disk_bios.sh <path_to_disk>"
	    exit -1
fi


qemu-kvm \
    -enable-kvm \
    -cpu host \
    -m 8G \
    -smp 8 \
    -boot c \
    -net user \
    -nic user,hostfwd=tcp::2222-:22 \
    -net nic \
    -msg timestamp=on \
    -drive format=raw,file=$1
