# Copyright Capes-OS Contributors:
#
# - Rémy Grünblatt <remy@grunblatt.org>
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

{ config, pkgs, lib, osConfig, ... }:

{
  home = {
    username = "candidat";
    homeDirectory = "/home/candidat";
    stateVersion = "23.11";

    activation = {
      installZealDocsets =
        let
          zeal-documentation = (pkgs.callPackage ./packages/zeal-documentation.nix { });
        in
        lib.hm.dag.entryAfter [ "writeBoundary" ] ''
          mkdir -p ~/.local/share/Zeal/Zeal/docsets;
          ln -sf ${zeal-documentation}/docsets ~/.local/share/Zeal/Zeal/docsets
        '';
    };
  };

  xfconf.settings = {
    xfce4-desktop = {
      "backdrop/screen0/monitorVirtual-1/workspace0/last-image" = "${osConfig.capesos.background}";
      "backdrop/screen0/monitorVirtual1/workspace0/last-image" = "${osConfig.capesos.background}";
    };
    xfwm4 = {
      "general/placement" = "0";
      "general/wrap_windows" = "false";
    };
  };

  services = {
    flameshot = {
      enable = true;
      settings = {
        General = {
          showStartupLaunchMessage = false;
        };
      };
    };
  };

  gtk = {
    enable = true;
    iconTheme = {
      name = "elementary-Xfce-dark";
      package = pkgs.elementary-xfce-icon-theme;
    };
    theme = {
      name = "yaru";
      package = pkgs.yaru-theme;
    };
    gtk3.extraConfig = {
      Settings = ''
        gtk-application-prefer-dark-theme=0
      '';
    };
    gtk4.extraConfig = {
      Settings = ''
        gtk-application-prefer-dark-theme=0
      '';
    };
  };

  systemd.user.targets.tray = {
    Unit = {
      Description = "Home Manager System Tray"; # See https://github.com/nix-community/home-manager/issues/2064
      Requires = [ "graphical-session-pre.target" ];
    };
  };

  services.gpg-agent.enable = true;
  programs = {
    gpg = {
      enable = true;
    };
    firefox = {
      enable = true;
      package = pkgs.wrapFirefox pkgs.firefox-bin-unwrapped {
        extraPolicies = {
          CaptivePortal = false;
          DisableFirefoxStudies = true;
          DisablePocket = true;
          DisableTelemetry = true;
          DisableFirefoxAccounts = false;
          NoDefaultBookmarks = false;
          OfferToSaveLogins = false;
          OfferToSaveLoginsDefault = false;
          PasswordManagerEnabled = false;
          FirefoxHome = {
            Search = true;
            Pocket = false;
            Snippets = false;
            TopSites = false;
            Highlights = false;
          };
          Homepage = {
            StartPage = "homepage";
            URL = "https://capes-nsi.org/";
          };
          OverrideFirstRunPage = "https://capes-nsi.org/";
          UserMessaging = {
            ExtensionRecommendations = false;
            SkipOnboarding = true;
          };
          ExtensionSettings = { };
          Extensions = {
            "Install" = [
              (pkgs.fetchurl {
                url = "https://addons.mozilla.org/firefox/downloads/latest/ublock-origin/latest.xpi";
                hash = "sha256-6O4/nVl6bULbnXP+h8HVId40B1X9i/3WnkFiPt/gltY=";
              })
            ];
          };
        };
      };
      profiles = {
        "Profil par défaut" = {
          settings = {
            "browser.translations.autoTranslate" = false;
            "browser.translations.automaticallyPopup" = false;
          };
          bookmarks = [
            {
              name = "CAPES NSI Bookmarks"; # Bookmark Folder
              toolbar = true;
              bookmarks =
                [
                  { name = "Documentation du système"; url = "https://capes-cafep-nsi.gitlab.io/OS/"; }
                  { name = "Capes NSI"; url = "https://capes-nsi.org/"; }
                  { name = "Serveur Web"; url = "http://webserver/"; }
                  { name = "PhpMyAdmin"; url = "http://phpmyadmin/"; }
                ];
            }
          ];
          search = {
            default = "zoTop";
            engines = {
              "Amazon.com".metaData.hidden = true;
              "Wikipedia (en)".metaData.hidden = true;
              "Bing".metaData.hidden = true;
              "DuckDuckGo".metaData.hidden = true;
              "zoTop" = {
                urls = [{
                  template = "https://zotop.zaclys.com/";
                  params = [
                    { name = "q"; value = "{searchTerms}"; }
                  ];
                }];
              };
            };
            force = true;
          };
        };
      };
    };
    home-manager = {
      enable = true;
    };
  };
}
