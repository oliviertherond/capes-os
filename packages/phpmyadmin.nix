# Copyright Capes-OS Contributors:
#
# - Rémy Grünblatt <remy@grunblatt.org>
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

{ stdenv, fetchurl, lib, zeal }:

stdenv.mkDerivation rec {
  pname = "phpMyAdmin";
  version = "5.2.0";

  src = fetchTarball {
    url = "https://files.phpmyadmin.net/phpMyAdmin/5.2.1/phpMyAdmin-5.2.1-all-languages.zip";
    sha256 = "sha256:1lyrkdmanw8jsb0zc63k3y3lw1lxjvs7r8zvj27xmba9a6lvjr98";
  };

  installPhase = ''
    mkdir -p $out
    cp -r * $out

    cp $out/config.sample.inc.php $out/config.inc.php

    echo "\$cfg['Servers'][\$i]['connect_type'] = 'tcp';" >> $out/config.inc.php
    echo "\$cfg['TempDir'] = '/var/lib/phpmyadmin';" >> $out/config.inc.php
    sed -i 's/localhost/127.0.0.1/' $out/config.inc.php
  '';
  meta = with lib; {
    description = "A web interface for MySQL and MariaDB";
    homepage = "https://www.phpmyadmin.net/";
    license = licenses.gpl2;
  };
}
