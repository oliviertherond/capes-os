# Copyright Capes-OS Contributors:
#
# - Rémy Grünblatt <remy@grunblatt.org>
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

{ stdenv, fetchurl, lib, zeal }:

stdenv.mkDerivation rec {
  pname = "zeal-documentation";
  version = "0.0.1";

  sourceRoot = ".";
  srcs = map
    (lang: fetchurl {
      url = "http://sanfrancisco.kapeli.com/feeds/${lang.name}.tgz";
      hash = lang.hash;
    }) [

    {
      name = "Bash";
      hash = "sha256:17m1mpvpniiqd8gjrhzjwnqw0ik302rj3l5hqrfby31xplwlc6yv";
    }

    {
      name = "C";
      hash = "sha256:0lmzm54an65n2pfrns0hgkf2lx7qpzl3s567by143knsw0vk57vv";
    }

    {
      name = "C++";
      hash = "sha256:1mj82fay5kzfkwxinv6g0sgww5fzdhdbhbx7zd1gjlxxz63wmxxl";
    }

    {
      name = "CSS";
      hash = "sha256:18rj2sq1wj73xllda6fdqndjrynm29s6k2lqa664a5vjy38v5wgc";
    }

    {
      name = "Flask";
      hash = "sha256:1xn5r36xgnazpq0agv9ikraj0mwcsxiimijl7rm3k5n9hl8xrbp8";
    }

    {
      name = "HTML";
      hash = "sha256:090mdwk95c1a0ah93z86ci1cq4vhw9ps2w7n2sbivzi3s6xym3nv";
    }

    {
      name = "Java_SE19";
      hash = "sha256:0f1zi2hmziv79cqcn53xh5w9rsp4skgbszb8n5zn3c387i7s7mcm";
    }

    {
      name = "LaTeX";
      hash = "sha256:1jjh0rq87vc8m6m2m1cqmnxc4fx0xvx3abn711q7r38d467fiqkh";
    }

    {
      name = "Markdown";
      hash = "sha256:1cybjbrdcbakq5qqdy71xn420vrv9h6550b9m257nmp53237m122";
    }

    {
      name = "Matplotlib";
      hash = "sha256:1l6dvvd0pfaz37nfw2r9mnrxykncrp9gf5kxs66r3kdfhrxcp6dc";
    }

    {
      name = "MySQL";
      hash = "sha256:1hjbjhgfswb3sqhwz0c9ng43g814pyslh7bxz00aykbphxaf3v3k";
    }

    {
      name = "NodeJS";
      hash = "sha256:1l29y4im71lyn3s35xdxfpv2m69z20db3vlz72rc6na5ylpm7x0m";
    }

    {
      name = "NumPy";
      hash = "sha256:1f0iynrhyapqx2pslkq0gnv7z8f58x7411z3mmnqq6v0acjwyfc1";
    }

    {
      name = "OCaml";
      hash = "sha256:05ahi9rcw5zmcw610q3k6skbni146y69r1yh06fwnksrrx6xd7mb";
    }

    {
      name = "PHP";
      hash = "sha256:1r7kdfqv4k4y3g33s29jfg75amkzq9971ifrip7xg2pa5n6vd2vx";
    }

    {
      name = "Python_3";
      hash = "sha256:1i38h1dmxkq8l1v2hr2nln5ffny35svbs8xsxx1ayd6y85sn3z9r";
    }

    {
      name = "SciPy";
      hash = "sha256:16xis4q6mwsrf1ina6c7g6biq9jv80dlhx2rp2pk9iq5r3vkad1z";
    }

    {
      name = "SQLite";
      hash = "sha256:0mvwq2yf7ck8nf2y88psvqv3lhvp6li9dcsa7xv6xhpy13r7xqfg";
    }

    {
      name = "Vim";
      hash = "sha256:16p3zcqwx9n3ishnpwp19wzzkm2fq08nnkns4sgdpl5jwylmzhvb";
    }
  ];

  buildInputs = [
    zeal
  ];

  installPhase = ''
    mkdir -p $out/docsets
    cp -r *.docset $out/docsets/
    ls -alh $out/docsets/
  '';
}
