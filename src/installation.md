# Installation

Trois options s'offrent à vous pour installer Capes-OS :

- Recommandé : Installation « pérenne » en machine virtuelle, via VirtualBox ;
- Avancé : Installation « live », via l'image `.iso` ;
- Avancé : Installation « pérenne », via l'image de disque (`.bios.img.zst` ou `.uefi.img.zst`), en uefi ou en bios.

Les prérequis concernant le stockage sont les suivants :

- Au moins 16Go d'espace libre pour l'image .iso (système live) (téléchargement) et autant sur le périphérique sur lequel cette image sera écrite.
- Au moins 12Go pour le téléchargement de l'image de machine virtuelle VirtualBox (taille de l'ova), 60Go pour son utilisation.
- Au moins 10Go pour le téléchargement des images de disque, et au moins 64Go pour leur écriture sur des périphériques ;

Nous conseillons l'utilisation de l'image de machine virtuelle VirtualBox.

## Image de machine virtuelle VirtualBox

Téléchargez l'image `.ova`, puis importez là dans VirtualBox. Par défaut, l'image utilise 4096MB de ram et 4 cœurs CPU, et son image disque décompressée est de 60Go.

## Image live `.iso`

Téléchargez l'image `.iso`, puis utilisez un utilitaire pour écrire cette image sur un périphérique disque (par exemple une clef USB). On peut utiliser par exemple :

- [Rufus](https://rufus.ie/fr/) (Windows)
- [Balena Etcher](https://etcher.balena.io/) (Windows, Linux, OS X)

Une fois l'image écrite, il sera possible de démarrer votre machine sur ce périphérique en le sélectionnant lors de l'amorçage de la machine. Il est probablement nécessaire de désactiver le secure-boot pour permettre ce démarrage.

## Images de disque

Bien que les images de disques (bios ou efi) soient disponibles au téléchargement, aucune documentation n'est fournie quant à l'utilisation de ces images. Il est en effet aisé de casser son système d'exploitation en effectuant une mauvaise manipulation lors de l'écriture de ces images sur des périphériques de stockage, et nous considérons donc que cette tâche est réservée aux utilisateurs et utilisatrices avancées, capable d'effectuer cette manipulation sans documentation.

# Mises à jour

Pour mettre à jour un système déjà installé (par exemple, une machine virtuelle VirtualBox) sans avoir à ré-installer une image complète, il est possible de suivre les instructions suivantes.

## Image live `iso`

Il n'est pas possible de mettre à jour le système sans recréer le périphérique au complet.

## VirtualBox

### Première mise à jour

Se déplacer, en root, dans `/etc/nixos` et lancer les commandes suivantes :

```
# git clone https://gitlab.com/Capes-Cafep-NSI/OS.git .
Clonage dans '.'...
[…]
Résolution des deltas: 100% (85/85), fait.
# nixos-generate-config
writing /etc/nixos/hardware-configuration.nix...
warning: not overwriting existing /etc/nixos/configuration.nix
```

Puis, éditer, le fichier `/etc/nixos/additional-configuration.nix` pour y faire figurer les lignes suivantes:

```
{ config, lib, pkgs, ...}:
{
    imports = [ ./hardware-configuration.nix ];
    boot.loader.grub.device = "/dev/sda"; # Remplacer si votre racine "/" n'est pas /dev/sda1
}
```

Enfin, ajouter ces fichiers dans le dépôt git **sans créer de commit** (cela permettra d'utiliser les commandes `git stash`, `git pull`, puis `git stash pop` pour récupérer les modifications distantes) :

```
# git add hardware-configuration.nix additional-configuration.nix
```

Une fois ces étapes effectuées, il sera possible de lancer la commande `nixos-rebuild switch` pour mettre à jour le système.

### Mises à jour suivantes

Se déplacer dans `/etc/nixos`, lancer un `git pull` pour récupérer les modifications depuis le dépôt, puis lancer un `nixos-rebuild switch`.

En cas d'erreur, ou pous revenir à la version précédente, lancer `nixos-rebuild switch --rollback`.

