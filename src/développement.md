# Développement

Pour générer les images, si vous souhaitez participer au développement par exemple, il est nécessaire d'installer [le gestionnaire de paquets Nix](https://nixos.org/download#download-nix), et d'[activer les fonctionnalités expérimentales « flakes » et « nix-command »](https://nixos.wiki/wiki/Flakes#Enable_flakes).

Une fois que ces opérations ont été effectuées, il suffit de cloner le dépôt et d'exécuter les commandes suivantes :

```
nix build .#iso                 # Pour compiler l'iso
nix build .#ova                 # Pour compiler l'image VirtualBox
nix build .#bios                # Pour compiler l'image de disque BIOS
nix build .#efi                 # Pour compiler l'image de disque UEFI
nix build .#                    # Pour compiler l'ensemble des images
```

Attention, cette compilation peut prendre beaucoup d'espace disque, de mémoire vive, de bande passante, et de temps.

Aucun support spécifique n'est prévu pour le développement, l'unique support concernant Capes-OS est un support sur les binaires disponibles sur le [site officiel](https://capes-nsi.org/environnement/).
