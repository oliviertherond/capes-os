# Utilisation

## Démarrage

Le nom d'utilisateur est `candidat` et le mot de passe `concours`. La disposition du clavier est `azerty`. Cet utilisateur est autorisé à utiliser `sudo` pour exécuter des commandes administrateurs (cela ne sera pas le cas le jour de l'examen).

## Serveur Web

Un serveur web (nginx) est lancé et est accessible à l'adresse [http://webserver/](http://webserver/) depuis le système. La racine de ce serveur web est située au chemin `/srv/http`, dans lequel un fichier `index.php` est déjà présent.

Ce serveur web donne aussi accès à PhpMyAdmin sur [http://phpmyadmin/](http://phpmyadmin/), avec l'identifiant `candidat` et le mot de passe `concours`.

## Serveur MySQL

Un serveur sql (mariadb) est lancé et est accessible sur `localhost`. Il est possible de s'y connecter avec l'identifiant `candidat` et le mot de passe `concours`, en utilisant par exemple la commande `mysql -p`.

## Serveur SSH

Un serveur ssh est lancé sur la machine. Par défaut, il n'accepte que les connexions par clef SSH pour l'utilisateur `candidat` (pas de login en root).

## Pare-feux

Un pare-feux est activé par défaut sur la machine. Il s'agit de `nftables`. Par défaut, il autorise tout trafic en sortie, et il autorise le port `22` (tcp) en entrée, le trafic `icmp` et `icmpv6`. Il est possible d'accéder aux règles précises en utilisant la commande `sudo nft list table inet nixos-fw`, et de le désactiver en utilisant la commande `sudo systemctl stop nftables`.