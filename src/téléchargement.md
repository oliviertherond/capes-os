# Téléchargement

Les liens de téléchargement pour les différentes versions du système sont les suivants :

- Image de machine virtuelle : [https://filesender.renater.fr/?s=download&token=675bfc77-2443-4712-b101-c030cfdc3e3e](https://filesender.renater.fr/?s=download&token=675bfc77-2443-4712-b101-c030cfdc3e3e)
- Image ```.iso``` de système live : [https://filesender.renater.fr/?s=download&token=94248680-94b1-43d6-ade9-1b54a47755ef](https://filesender.renater.fr/?s=download&token=94248680-94b1-43d6-ade9-1b54a47755ef)
- Image disque ```bios``` : [https://filesender.renater.fr/?s=download&token=55532271-c279-4e7c-9156-636f3a885214](https://filesender.renater.fr/?s=download&token=55532271-c279-4e7c-9156-636f3a885214)
- Image disque ```efi``` : [https://filesender.renater.fr/?s=download&token=d4549bae-2911-487d-90c6-04ee2f6c2b67](https://filesender.renater.fr/?s=download&token=d4549bae-2911-487d-90c6-04ee2f6c2b67)

