# Copyright Capes-OS Contributors:
#
# - Rémy Grünblatt <remy@grunblatt.org>
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

{ config, lib, pkgs, ... }:
let
  zeal-documentation = (pkgs.callPackage ./packages/zeal-documentation.nix { });
  phpmyadmin = (pkgs.callPackage ./packages/phpmyadmin.nix { });
  filius = pkgs.callPackage ./packages/filius.nix { };
  filius-desktop-icon =
    pkgs.makeDesktopItem
      {
        name = "Filius";
        desktopName = "Filius";
        categories = [ "Network" "Science" ];
        exec = "filius";
      };
in
{
  environment.enableDebugInfo = true;
  users = {
    users = {
      root = {
        password = "concours";
      };
      candidat = {
        isNormalUser = true;
        description = "Candidat";
        extraGroups = [ "wheel" "networkmanager" "candidat" "wireshark" ];
        password = "concours";
      };
    };
    groups = {
      candidat = { };
    };
  };

  environment.etc."current-system-packages".text =
    let
      packages = builtins.map (p: "${p.name}") config.environment.systemPackages;
      sortedUnique = builtins.sort builtins.lessThan (lib.unique packages);
      formatted = builtins.concatStringsSep "\n" sortedUnique;
    in
    formatted;

  environment.systemPackages = with pkgs;
    [
      # Éditeurs et Bureautique
      emacs
      libreoffice
      mdbook
      pandoc
      texlive.combined.scheme-full
      pdfpc
      texstudio
      vim
      (vscode-with-extensions.override {
        vscode = vscodium;
        vscodeExtensions = with vscode-extensions; [
          ms-ceintl.vscode-language-pack-fr
          ms-python.python
          ocamllabs.ocaml-platform
        ];
      })
      # Divers
      zeal
      zeal-documentation
      tmux
      htop
      unzip
      flameshot
      ripgrep
      # Calcul
      octave
      sage
      libqalculate
      qalculate-qt
      # Python
      (python3.withPackages (ps: with ps; [
        flask
        imageio
        jupyterlab
        matplotlib
        networkx
        numpy
        pandas
        pandas
        pillow
        pip
        pydot
        qdarkstyle
        requests
        scipy
        selenium
        spyder
        spyder-kernels
        sqlite
        sympy
      ]))
      # Dataviz
      graphviz
      gnuplot
      # Contrôle de versions
      git
      subversion
      # Web
      firefox
      # Javascript
      nodePackages.npm
      nodejs
      # PHP
      php
      phpmyadmin
      # SQL
      sqlite
      sqlitebrowser
      # Graphisme
      gimp-with-plugins
      inkscape-with-extensions
      drawio
      # Audio & Multimédia
      audacity
      vlc
      # Réseau
      iperf3
      wireshark
      dnsutils
      mtr
      curl
      wget
      # gns3-gui
      # gns3-server
      filius
      filius-desktop-icon
      filezilla
      # C
      gcc
      clang
      valgrind
      gdb
      # Ocaml
      ocaml
      ocamlformat
      ocamlPackages.utop
      ocamlPackages.merlin
      ocamlPackages.ocp-indent
      ocamlPackages.utop
      ocamlPackages.odoc
      ocamlPackages.ocaml-lsp
      # Java
      jdk17
      # Système
      blueman
      drawing
      font-manager
      orca
      pavucontrol
      wmctrl
      xclip
      xcolor
      xcolor
      xdo
      xdotool
      xfce.catfish
      xfce.orage
      xfce.xfburn
      xfce.xfce4-appfinder
      xfce.xfce4-clipman-plugin
      xfce.xfce4-cpugraph-plugin
      xfce.xfce4-dict
      xfce.xfce4-fsguard-plugin
      xfce.xfce4-genmon-plugin
      xfce.xfce4-netload-plugin
      xfce.xfce4-panel
      xfce.xfce4-pulseaudio-plugin
      xfce.xfce4-systemload-plugin
      xfce.xfce4-weather-plugin
      xfce.xfce4-whiskermenu-plugin
      xfce.xfce4-xkb-plugin
      xfce.xfdashboard
      xfce.xfwm4
      xorg.xev
      xsel
    ];

  hardware = {
    pulseaudio.enable = false;
    bluetooth.enable = true;
  };

  programs = {
    wireshark.enable = true;
    dconf.enable = true;
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
  };

  networking = {
    hostName = "Capes-OS";
    networkmanager = {
      enable = true;
    };

    hosts = {
      "127.0.0.1" = [ "phpmyadmin" "webserver" ];
    };

    nftables = {
      enable = true;
    };

    firewall = {
      enable = true;
    };
  };

  nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  capesos = {
    background = pkgs.callPackage ./packages/background.nix { sourceInfo = config.capesos.sourceInfo; };
  };

  services = {
    blueman.enable = true;
    pipewire = {
      enable = true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
      pulse.enable = true;
    };
    xserver = {
      layout = "fr";
      enable = true;
      desktopManager = {
        xterm.enable = false;
        xfce.enable = true;
      };
      displayManager = {
        defaultSession = "xfce";
        lightdm = {
          background = config.capesos.background;
        };

      };
    };

    mysql = {
      enable = true;
      initialScript = ./scripts/mysql_initial_config.sql;
      package = pkgs.mariadb;
    };

    phpfpm =
      let
        settings = {
          "listen.owner" = config.services.nginx.user;
          "pm" = "dynamic";
          "pm.max_children" = 32;
          "pm.max_requests" = 500;
          "pm.start_servers" = 2;
          "pm.min_spare_servers" = 2;
          "pm.max_spare_servers" = 5;
          "php_admin_value[error_log]" = "stderr";
          "php_admin_flag[log_errors]" = true;
          "catch_workers_output" = true;
        };
        user = "candidat";
        phpEnv."PATH" = lib.makeBinPath [ pkgs.php ];
      in
      {
        pools = {
          pma = {
            inherit settings user phpEnv;
          };
          candidat = {
            inherit settings user phpEnv;
          };
        };
      };

    nginx = {
      enable = true;
      virtualHosts = {
        "webserver" = {
          root = "/srv/http";
          locations = {
            "~ ^.+?\\.php(/.*)?$" = {
              extraConfig = ''
                try_files $fastcgi_script_name =404;
                fastcgi_split_path_info ^(.+\.php)(/.*)$;
                fastcgi_pass unix:${config.services.phpfpm.pools.candidat.socket};
                fastcgi_index index.php;
                include ${pkgs.nginx}/conf/fastcgi_params;
                include ${pkgs.nginx}/conf/fastcgi.conf;
              '';
            };
            "/" = {
              index = "index.php";
            };
          };
        };
        "phpmyadmin" = {
          root = "${phpmyadmin}";
          locations = {
            "~ ^.+?\\.php(/.*)?$" = {
              extraConfig = ''
                try_files $fastcgi_script_name =404;
                fastcgi_split_path_info ^(.+\.php)(/.*)$;
                fastcgi_pass unix:${config.services.phpfpm.pools.pma.socket};
                fastcgi_index index.php;
                include ${pkgs.nginx}/conf/fastcgi_params;
                include ${pkgs.nginx}/conf/fastcgi.conf;
              '';
            };
            "/" = {
              index = "index.php";
            };
          };
        };
      };
    };

    openssh = {
      enable = true;
      settings = {
        PasswordAuthentication = false;
        PermitRootLogin = "no";
      };
    };
  };

  systemd.tmpfiles.rules =
    let greeter = ''<?php echo("Bonjour ! La racine de ce serveur web est située au chemin /srv/http/, qui contient ce fichier index.php !") ?>'';
    in
    [
      "d /srv/http 0775 candidat nginx"
      "d /var/lib/phpmyadmin 0755 candidat candidat"
      "f /srv/http/index.php 0755 candidat candidat - ${greeter}"
    ];

  sound = {
    enable = true;
    mediaKeys.enable = true;
  };

  i18n = {
    defaultLocale = "fr_FR.UTF-8";
  };

  environment.variables = {
    LANGUAGE = lib.mkForce "fr_FR.UTF-8";
    LANG = lib.mkForce "fr_FR.UTF-8";
    LC_ALL = lib.mkForce "fr_FR.UTF-8";
  };

  time = {
    timeZone = "Europe/Paris";
  };

  boot = {
    kernelParams = [ "quiet" "splash" ];
    plymouth = {
      enable = true;
      themePackages = with pkgs; [ adi1090x-plymouth-themes ];
      theme = "pixels";
    };
    consoleLogLevel = 0;
    initrd = {
      verbose = false;
    };

    loader = {
      timeout = lib.mkForce 1;
      grub = {
        extraConfig = ''
          set timeout_style=hidden
        '';
        splashImage = null;
      };
    };

  };

  system.stateVersion = "23.11";
}
